#ifndef DATASET_HPP
#define DATASET_HPP

#include <iosfwd>
#include <jules/base/const_vector.hpp>

namespace tourist
{

class dataset
{
public:
  explicit dataset(std::istream& is);

  auto neighbors(std::size_t index) const -> jules::const_vector<std::size_t>;

  auto size() const -> std::size_t;

private:
  std::vector<jules::const_vector<std::size_t>> neighborhoods;
};
}

#endif // DATASET_HPP
