#pragma once

#include "dataset.hpp"

#include <deque>
#include <unordered_map>
#include <vector>

namespace tourist
{
struct attractor_data
{
  std::vector<std::size_t> sources;
  std::vector<std::size_t> cycle;

  auto operator<(const attractor_data& other) const -> bool;
  auto operator==(const attractor_data& other) const -> bool;
};

class attractor_helper
{
public:
  attractor_helper(std::size_t window_size);

  // returns false if the cycle is closed.
  auto add_next(std::size_t index) -> bool;

  auto has(std::size_t index) const -> bool;

  auto canon() && -> attractor_data;

private:
  struct state
  {
    std::size_t step;
    std::deque<std::size_t> memory;
  };

  std::vector<std::size_t> cycle_;
  std::deque<std::size_t> memory_;
  std::unordered_map<std::size_t, std::vector<state>> vstates_;
  std::size_t window_size_;
};

struct walk_result
{
  std::size_t window_size;
  std::vector<attractor_data> attractors;
};

class system
{
public:
  system(dataset data);

  auto iterate(std::size_t window_size) const -> walk_result;

  auto size() const { return data_.size(); }

private:
  const dataset data_;
};
}
