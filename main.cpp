#include "tourist.hpp"

#include <iostream>

int main()
{
  const auto system = tourist::system(tourist::dataset(std::cin));

  for (std::size_t window_size = 1; window_size < system.size(); ++window_size)
  {
    std::cerr << "with window_size " << window_size << " there are ";
    const auto result = system.iterate(window_size);
    std::cerr << result.attractors.size() << " attractors\n";

    auto count = std::vector<std::size_t>(system.size(), 0u);
    for (const auto& attractor : result.attractors)
      for (auto elem : attractor.cycle)
        ++count[elem];

    for (std::size_t i = 0; i < system.size(); ++i)
      std::cout << window_size << ' ' << i << ' ' << count[i] << '\n';
  }
}
