#include "tourist.hpp"

#include <algorithm>

namespace tourist
{
attractor_helper::attractor_helper(std::size_t window_size)
  : window_size_{window_size}
{
}

auto attractor_helper::add_next(std::size_t index) -> bool
{
  // Put into memory
  memory_.push_front(index);
  if (memory_.size() > window_size_)
    memory_.pop_back();

  auto& states = vstates_[index];

  if (memory_.size() == window_size_)
    states.push_back({cycle_.size(), memory_});
  else
    states.push_back({cycle_.size(), {}});

  cycle_.push_back(index);

  if (memory_.size() < window_size_)
    return true;

  if (states.size() < 3)
    return true;

  const auto current = states.rbegin();
  auto previous = std::next(current);
  for (; previous != states.rend(); ++previous)
    if (current->memory == previous->memory)
      break;

  if (previous == states.rend())
    return true;

  auto new_cycle = decltype(cycle_)(cycle_.begin() + previous->step, cycle_.end() - 1);
  cycle_ = std::move(new_cycle);

  return false;
}

auto attractor_helper::has(std::size_t index) const -> bool
{
  for (auto it = memory_.begin(); it != memory_.end(); ++it)
    if (*it == index)
      return true;
  return false;
}

// is it really canon? what if two minimum?
auto attractor_helper::canon() && -> attractor_data
{
  const auto it = std::min_element(cycle_.begin(), cycle_.end());
  std::rotate(cycle_.begin(), it, cycle_.end());
  return {{}, std::move(cycle_)};
}

auto attractor_data::operator<(const attractor_data& other) const -> bool
{
  return cycle < other.cycle;
}

auto attractor_data::operator==(const attractor_data& other) const -> bool
{
  return cycle == other.cycle;
}

system::system(dataset data)
  : data_{std::move(data)}
{
}

auto system::iterate(std::size_t window_size) const -> walk_result
{
  auto attractors = std::vector<attractor_data>();

  for (auto i = 0ul; i < data_.size(); ++i)
  {
    auto j = i;
    auto a = attractor_helper{window_size};
    while (a.add_next(j))
    {
      const auto nei = data_.neighbors(j);
      for (auto next : nei)
      {
        if (a.has(next))
          continue;
        j = next;
        break;
      }
    }

    auto canon = std::move(a).canon();

    const auto pos = std::lower_bound(attractors.begin(), attractors.end(), canon);
    if (pos == attractors.end())
    { // do not exist, and must stay at the end.
      canon.sources.push_back(i);
      attractors.push_back(std::move(canon));
    }
    else if (canon < *pos)
    { // do not exist, and must stay at pos.
      canon.sources.push_back(i);
      attractors.insert(pos, std::move(canon));
    }
    else
    { // already existent
      pos->sources.push_back(i);
    }
  }

  return {window_size, std::move(attractors)};
}
}
