#include "dataset.hpp"

#include <algorithm>
#include <iostream>
#include <sstream>

#include <jules/array/all.hpp>

namespace
{
static auto read_data(std::istream& is)
{
  auto result = std::vector<jules::vector<double>>();
  auto tmp = std::vector<double>();
  auto dimension = 0ul;

  auto line = std::string();
  while (std::getline(is, line))
  {
    if (line.empty())
      continue;

    std::stringstream ss(line);
    double point;

    tmp.reserve(dimension);
    while (ss >> point)
      tmp.push_back(point);

    const auto current_dimension = tmp.size();
    if (current_dimension == 0)
      continue;

    if (dimension != 0ul && current_dimension != dimension)
      throw std::runtime_error{"invalid dataset"};
    dimension = current_dimension;

    result.emplace_back(tmp.begin(), tmp.end());
    tmp.clear();
  }

  return result;
}
}

namespace tourist
{

dataset::dataset(std::istream& is)
{
  const auto data = read_data(is);
  const auto nsamples = data.size();

  auto distances = std::vector<double>();
  distances.reserve(nsamples * (nsamples - 1) / 2);

  for (auto i = std::size_t{0}; i < nsamples; ++i)
    for (auto j = i + 1; j < nsamples; ++j)
      distances.push_back(sum(square(data[i] - data[j])));

  const auto get_distance = [&](std::size_t i, std::size_t j) {
    if (i == j)
      return std::numeric_limits<double>::infinity();
    if (i > j)
      std::swap(i, j);
    const auto pos = (nsamples * (nsamples - 1) / 2) -
                     (nsamples - i) * ((nsamples - i) - 1) / 2 + j - i - 1;
    return distances.at(pos);
  };

  neighborhoods.reserve(nsamples);

  namespace view = jules::range::view;
  auto tmp = std::vector<std::pair<std::size_t, double>>(nsamples);

  for (auto i = std::size_t{0}; i < nsamples; ++i)
  {
    for (auto j = std::size_t{0}; j < nsamples; ++j)
      tmp[j] = std::make_pair(j, get_distance(i, j));

    auto sampled = tmp | view::sample(tmp.size()) | view::bounded;
    auto tmp1 = decltype(tmp)(sampled.begin(), sampled.end());
    tmp = std::move(tmp1);

    std::sort(tmp.begin(), tmp.end(),
              [](const auto& x, const auto& y) { return x.second < y.second; });
    auto indexes = tmp | view::transform([](const auto& x) { return x.first; });
    neighborhoods.emplace_back(indexes.begin(), indexes.end());
  }
}

auto dataset::neighbors(std::size_t index) const -> jules::const_vector<std::size_t>
{
  return neighborhoods[index];
}

auto dataset::size() const -> std::size_t { return neighborhoods.size(); }
}
